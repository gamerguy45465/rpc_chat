package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"reflect"
	"sort"
	"strings"
	"sync"
	"time"
	"unicode"
	"unicode/utf8"
)

const (
	MsgRegister = iota
	MsgList
	MsgCheckMessages
	MsgTell
	MsgSay
	MsgQuit
	MsgShutdown
)

var mutex sync.Mutex
var messages map[string][]string
var shutdown chan struct{}

/*************Send and Receive***************************/

/*func sendAndReceive(address string, message []byte) error {
	conn, err := net.Dial("tcp", address)
	if err != nil {
		fmt.Println("There was an error")
		return err

	}

	//conv_msg := convertUint16(message)
	conn.Write(message)

	read_byte := make([]byte, 256)
	n, _ := conn.Read(read_byte)
	n++
	fmt.Printf("%s\n", read_byte)

	return nil

}

/*************Send and Receive***************************/

func server(listenAddress string) {
	shutdown = make(chan struct{}, 2)
	messages = make(map[string][]string)

	// set up network listen and accept loop
	// to receive RPC requests and dispatch each
	// in its own goroutine

	ln, err := net.Listen("tcp", listenAddress)

	fmt.Println("Listening to: ", listenAddress, " : ", ln)

	if err != nil {
		fmt.Println("Error")
		return
	}

	go func() {
		for {
			conn, err := ln.Accept()
			if err != nil {
				fmt.Println("Error Occured")
				break
			}

			//fmt.Println("Established Connection: ", conn)

			go dispatch(conn)

		}

	}()

	<-shutdown

	fmt.Println("The server has shut down")
	//wait for a shutdown request

	time.Sleep(100 * time.Millisecond)

	//ln.Close()

	return

}

func client(serverAddress string, username string) {
	RegisterRPC(serverAddress, username)

	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Welcome", "please type something and press enter")
	go receiveHandler(serverAddress, username)
	for scanner.Scan() {
		line := scanner.Text()

		if i := strings.Index(line, "//"); i >= 0 {
			line = line[:i]
		}

		if len(strings.TrimSpace(line)) == 0 {
			continue
		}
		line += "\n"

		var command string
		var message string
		var target string

		fmt.Sscanf(line, "%s %s\n", &command, &target)

		if command == "say" {
			for i := 3; i < len(line); i++ {
				if i == 3+(len(command)-1) {
					line = line[i-1:]
					break
				}
			}

		}

		if command == "tell" {
			for i := 5; i < len(line); i++ {
				if i == 5+(len(target)-1) {
					line = line[i+2:]
					break
				}
			}

		}

		message = line

		if command == "list" {
			//fmt.Println(ListRPC(serverAddress))
			fmt.Println(ListRPC(serverAddress, target))
		} else if command == "register" {
			RegisterRPC(serverAddress, target)
		} else if command == "tell" {
			TellRPC(serverAddress, username, target, message)
		} else if command == "say" {
			SayRPC(serverAddress, username, message)
		} else if command == "quit" {
			QuitRPC(serverAddress, username)
			goto quit // I know this is considered bad practice, but it's what I could do to get the client to stop when it quits
		} else if command == "shutdown" {
			ShutdownRPC(serverAddress)
			//goto quit
		} else {
			fmt.Println("Error, not a valid command. Plase try again")
		}
	}

quit:
	return

}

func receiveHandler(serverAddress, user string) {

	for {

		time.Sleep(time.Second)
		//response, _ :=
		CheckMessagesRPC(serverAddress, user)

	}

}

func Gravel(conn net.Conn) []byte {
	// We have to create a byte array to return, use make to create it Ex: make([]byte, 0)
	message := make([]byte, 0)
	// Track the length of the message, create a message length variable and set it equal to zero
	message_len := 0
	// Do this next bit twice
	// the first time, do it to get the length fo the message
	// create a for loop that runs indefinetely

	for {
		// Create a temporary buffer, Ex: buf := make([]byte, 256)
		buffer := make([]byte, 256)

		// Load stuff into buffer using num, _ := conn.Read(buf). num is the number of items read
		num, err := conn.Read(buffer)

		if err == io.EOF {
			fmt.Println("Connection was closed for some reason")
			return buffer
		}

		// append that number of items into the byte array that we just created. Use something like this msg = append(msg, buff[:num]...). buff is length 256, so all the rest would be zeros
		message = append(message, buffer[:num]...)

		// so we split it at buf[:num] (above comment, no need to write code for this comment)

		//if we have two items, those two items specify how long the total message will be.
		// so we break

		if len(message) > 1 {
			// Ex for here: msg_len = int(MrSandmanMakeMeAUint16Please(&msg))
			message_len = int(convertUint16(message))
			break
		}

	}

	new_length := convertTwoByteArray(uint16(message_len))

	// Do it again, repeating until we have the required number of bytes read
	conn.Write(new_length)

	for len(message) < message_len {

		buffer := make([]byte, 256)
		num, _ := conn.Read(buffer)

		message = append(message, buffer[:num]...)

	}
	return message
}

/* CONVERTING FUNCTIONS */

func trimFirstRune(s string) string {
	_, i := utf8.DecodeRuneInString(s)
	return s[i:]
}

func trimRune(s string, index int) string {
	return s[index:]
}

func convertUint16(message []byte) (result uint16) {
	if len(message) == 1 {
		result, message = (uint16((message)[0]) << 8), (message)[0:]
	} else {

		result, message = (uint16((message)[0])<<8)+uint16((message)[1]), (message)[2:]
	}

	return result

}

func convertTwoByteArray(n uint16) []byte {
	return []byte{byte((n >> 8) & 0xff), byte(n & 0xff)}
}

func reverse(s interface{}) {
	n := reflect.ValueOf(s).Len()
	swap := reflect.Swapper(s)
	for i, j := 0, n-1; i < j; i, j = i+1, j-1 {
		swap(i, j)
	}
}

func fixByteArray(b *[]byte) {
	reverse(*b)
}

func MtResponse() []byte {
	return []byte{}

}

func pop(message []byte, index int) []byte {
	var new_message []byte
	if len(message) == index {
		new_message = message[:len(message)-1]
	} else {
		new_message = append((message)[:index], (message)[index+1:]...)
	}
	return new_message
}

func dispatch(conn net.Conn) {
	/*
		handle a single incoming request:
			1. Read the length (uint16)
			2. Read the entire message into a []byte
			3. From the message, parse the message type (uint16)
			4. Call the appropriate server stub, giving it the remainder of the request []byte and collecting the response []byte
			5. Write the message length (uint16)
			6. Write the message []byte
			7. Close the connection


			On any error, be sure to close the connection, log a message and return (a request error should not kill the entire server)
	*/
	// To get length, and also read message into a []byte, call gravel and pass conn as parameter
	// For step 3, you can do something like msg_type := MrSandmanMakeMeAUint16Please(&msg)
	//

	message := Gravel(conn)

	msg_type := convertUint16(message)

	byte_response := []byte{}

	/************** FOR PARSING ********************/

	temp_byte := make([]byte, 2000)

	msg, _ := conn.Read(temp_byte)

	read_byte := make([]byte, msg)
	read_byte = temp_byte[:msg]

	/**************** FOR PARSING ******************/

	switch msg_type {
	case MsgRegister:
		byte_response = msgRegister(read_byte)
	case MsgList:
		byte_response = msgList(read_byte, conn)
	case MsgCheckMessages:
		byte_response = msgMessages(read_byte)
	case MsgTell:
		user_byte := make([]byte, 510)

		conn.Read(user_byte)
		time.Sleep(100 * time.Millisecond)
		sender_byte := make([]byte, 510)

		conn.Read(sender_byte)

		message_byte := make([]byte, 510)
		conn.Read(message_byte)

		byte_response = msgTell(message_byte, user_byte, sender_byte)
	case MsgSay:
		time.Sleep(time.Second)
		user_byte := make([]byte, 510)

		conn.Read(user_byte)

		byte_response = msgSay(user_byte, read_byte)
	case MsgQuit:
		byte_response = msgQuit(read_byte)
	case MsgShutdown:

		byte_response = msgShutdown()

	default:
		byte_response = MtResponse()

	}

	//fmt.Printf("%s <-The Message\n", byte_response)

	conn.Write(byte_response)

	//fmt.Println(messages)

	conn.Close()

	return

}

/* DECODE AND ENCODE */

func decode(message []byte) (result []string, status *string) {

	length := pop(message, 2)
	//lengthU16 := len(message)

	lengthU16 := convertUint16(length)

	byte_arr := []byte{}

	for lengthU16 > 0 {
		byte_arr = pop(message, 2)

		sub_u16 := convertUint16(byte_arr)

		sub_str := pop(message, int(sub_u16))

		result = append(result, string(sub_str))

		lengthU16--
	}
	tString := new(string)
	*tString = ""

	status = tString

	return

}

func encode(messages []string, err error) []byte {
	string_amount := uint16(len(messages))
	res := convertTwoByteArray(string_amount)

	for _, new_str := range messages {
		tmp := []byte(new_str + " ")
		ln := uint16(len(tmp))
		two_byte_ln := convertTwoByteArray(uint16(ln))
		res = append(res, two_byte_ln...)
		res = append(res, tmp...)
	}
	if err != nil {
		new_error := err.Error()
		res = append(res, convertTwoByteArray(uint16(len(new_error)))...)
		res = append(res, []byte{0, 0}...)
	} else {
		res = append(res, []byte{0, 0}...)
	}

	res = append(convertTwoByteArray(uint16(len(res))), res...)
	return res
}

/* BYTE MESSAGE HANDLERS (Server Stubs) */

func msgRegister(message []byte) []byte {

	//data, _ := decode(message)
	data := string(message)

	err := Register(data)
	return encode([]string{data}, err)
	//return []byte{}

}

func msgList(message []byte, conn net.Conn) []byte {
	strs := List()

	b_arr := encode(strs, nil)

	//conn.Write(b_arr)

	return b_arr
}

func msgMessages(message []byte) []byte {
	//data, _ := decode(message)
	data := []string{string(message)}
	//fmt.Println(data)
	strs := CheckMessages(data[0])

	e := encode(strs, nil)
	//fmt.Println(e)
	return e
}

func msgTell(message []byte, sender []byte, user []byte) []byte {
	//data_1, _ := decode(sender)
	//data_2, _ := decode(user)
	//data_3, _ := decode(message)

	var new_message []byte

	for i := 0; i < len(message); i++ {
		if message[i] != 0 {
			new_message = append(new_message, message[i])

		}
	}

	data_1 := string(sender)
	data_2 := string(user)
	data_3 := string(new_message)

	var from string
	var to string
	//var con string

	for i := 0; unicode.IsLetter(rune(data_1[i])) || unicode.IsDigit(rune(data_1[i])); i++ {
		from += string(data_1[i])

	}
	for i := 0; unicode.IsLetter(rune(data_2[i])) || unicode.IsDigit(rune(data_2[i])); i++ {
		to += string(data_2[i])

	}
	/*for i := 0; unicode.IsLetter(rune(data_3[i])) || unicode.IsDigit(rune(data_3[i])); i++ {
		con += string(data_3[i])

	} */

	Tell(from, to, data_3)
	return MtResponse()
}

func msgSay(user, message []byte) []byte {
	//data, _ := decode(message)

	var new_user []byte
	for i := 0; i < len(user); i++ {
		if user[i] != 0 {
			new_user = append(new_user, user[i])
		}
	}

	data_1 := string(new_user)
	data_2 := string(message)
	Say(data_1, data_2)
	return MtResponse()
}

func msgQuit(message []byte) []byte {
	//data, _ := decode(message)
	data := string(message)
	Quit(data)
	return MtResponse()
}

func msgShutdown() []byte {
	Shutdown()
	return MtResponse()
}

/* CLIENT HANDLERS (Client Stubs) */

func RegisterRPC(server, user string) ([]string, error) {
	conn, err := net.Dial("tcp", server)
	if err != nil {
		fmt.Println("Unable to establish connection", conn)
		return []string{"Error logging in"}, err
	}

	n := convertTwoByteArray(MsgRegister)

	conn.Write(n)

	time.Sleep(100 * time.Millisecond)

	excess := make([]byte, 500)

	conn.Read(excess)

	time.Sleep(100 * time.Millisecond)

	m := []byte(user)

	conn.Write(m)

	//reg_handle := Register(user)

	//fmt.Println("Here")

	return []string{"Login Successful"}, err

}

func ListRPC(server string, user string) ([]string, error) {
	conn, err := net.Dial("tcp", server)

	if err != nil {
		fmt.Println("Unable to establish connection", conn)
		return []string{"Error listing"}, err
	}

	time.Sleep(100 * time.Millisecond)

	n := convertTwoByteArray(MsgList)

	conn.Write(n)

	time.Sleep(100 * time.Millisecond)

	conn.Write(n)

	time.Sleep(100 * time.Millisecond)

	u_arr := []byte(user)

	conn.Write(u_arr)

	time.Sleep(100 * time.Millisecond)

	b_arr := make([]byte, 256)

	conn.Read(b_arr)

	return []string{string(b_arr)}, err

}

func TellRPC(server, user, target, message string) ([]string, error) {
	conn, err := net.Dial("tcp", server)
	if err != nil {
		return []string{"Error, unable to connect to the server"}, err
	}

	n := convertTwoByteArray(MsgTell)

	conn.Write(n)

	time.Sleep(100 * time.Millisecond)
	excess := make([]byte, 2)

	l, _ := conn.Read(excess)
	l++

	//fmt.Println(int(excess[1]))

	time.Sleep(100 * time.Millisecond)

	time.Sleep(100 * time.Millisecond)
	conn.Write([]byte(message))

	time.Sleep(100 * time.Millisecond)

	msg_byte := []byte(message)

	conn.Write(msg_byte)

	time.Sleep(time.Second)

	user_byte := []byte(user)

	conn.Write(user_byte)

	time.Sleep(time.Second)

	target_byte := []byte(target)

	conn.Write(target_byte)

	time.Sleep(100 * time.Millisecond)

	conn.Write(msg_byte)

	time.Sleep(100 * time.Millisecond)

	ret_arr := make([]byte, 2000)

	conn.Read(ret_arr)

	return []string{string(ret_arr)}, nil

}
func CheckMessagesRPC(server, user string) (string, error) {
	conn, err := net.Dial("tcp", server)
	if err != nil {
		return "", err
	}
	n := convertTwoByteArray(MsgCheckMessages)
	conn.Write(n)

	time.Sleep(100 * time.Millisecond)

	excess := make([]byte, 2)

	l, _ := conn.Read(excess)

	time.Sleep(100 * time.Millisecond)

	m := []byte(user)

	time.Sleep(100 * time.Millisecond)
	conn.Write(m)

	l++

	r_messages := make([]byte, 5000)
	time.Sleep(100 * time.Millisecond)

	o, err := conn.Read(r_messages)

	if r_messages[1] != 4 {

		fmt.Println(string(r_messages[6:]))
	}

	o++

	return string(r_messages), err

}

func SayRPC(server, user, message string) ([]string, error) {
	conn, err := net.Dial("tcp", server)

	if err != nil {
		fmt.Println("There was an error")
		return []string{"There was an error"}, err
	}

	n := convertTwoByteArray(MsgSay)

	conn.Write(n)

	time.Sleep(100 * time.Millisecond)
	excess := make([]byte, 2)

	l, _ := conn.Read(excess)
	l++

	time.Sleep(100 * time.Millisecond)

	time.Sleep(100 * time.Millisecond)
	conn.Write([]byte(message))

	time.Sleep(100 * time.Millisecond)

	msg_byte := []byte(message)

	conn.Write(msg_byte)

	time.Sleep(time.Second)

	user_byte := []byte(user)

	conn.Write(user_byte)

	time.Sleep(time.Second)

	ret_byte := make([]byte, 500)

	conn.Read(ret_byte)

	return []string{""}, err

}

func QuitRPC(serverAddress, user string) {
	conn, err := net.Dial("tcp", serverAddress)

	if err != nil {
		fmt.Println("Error")
		return
	}

	n := convertTwoByteArray(MsgQuit)

	conn.Write(n)

	excess := make([]byte, 255)
	conn.Read(excess)

	conn.Write([]byte(user))

	conn.Write([]byte(user))

	read_byte := make([]byte, 256)

	conn.Read(read_byte)

}

func ShutdownRPC(serverAddress string) {
	conn, err := net.Dial("tcp", serverAddress)

	if err != nil {
		fmt.Println("There was an error")
		return
	}

	n := convertTwoByteArray(MsgShutdown)
	conn.Write(n)

	excess := make([]byte, 256)

	conn.Read(excess)

	conn.Write([]byte(serverAddress))

	conn.Write([]byte(serverAddress))

	//read_byte := make([]byte, 256)

	//conn.Read(read_byte)

	conn.Close()

	log.Printf("The server has shut down")
}

/* GENERIC HANDLERS */

func Register(user string) error {
	if len(user) < 1 || len(user) > 20 {
		return fmt.Errorf("Register: user must be between 1 and 20 letters")
	}
	for _, r := range user {
		if !unicode.IsLetter(r) && !unicode.IsDigit(r) {
			return fmt.Errorf("Register: user must only contain letters and digits")

		}
	}
	mutex.Lock()
	defer mutex.Unlock()

	msg := fmt.Sprintf("*** %s has logged in", user)
	for target, queue := range messages {
		messages[target] = append(queue, msg)
	}

	//user += " "
	messages[user] = nil

	return nil
}

func List() []string {
	mutex.Lock()
	defer mutex.Unlock()

	var users []string
	for target := range messages {
		users = append(users, target)
	}
	sort.Strings(users)
	fmt.Println(users)

	return users
}

func CheckMessages(user string) []string {
	mutex.Lock()
	defer mutex.Unlock()

	if queue, present := messages[user]; present {
		messages[user] = nil
		return queue
	} else {
		return []string{"*** You are not logged in, " + user}
	}
}

func Tell(user, target, message string) {
	mutex.Lock()
	defer mutex.Unlock()

	msg := fmt.Sprintf("%s tells you %s", user, message)

	if queue, present := messages[target]; present {
		messages[target] = append(queue, msg)
	} else if queue, present := messages[user]; present {
		messages[user] = append(queue, "*** NO such user: "+target)
	}

}

func Say(user, message string) {
	mutex.Lock()
	defer mutex.Unlock()

	msg := fmt.Sprintf("%s says %s", user, message)
	for target, queue := range messages {
		messages[target] = append(queue, msg)
	}
}

func Quit(user string) {
	mutex.Lock()
	defer mutex.Unlock()

	msg := fmt.Sprintf("*** %s had logged out", user)
	log.Print(msg)
	for target, queue := range messages {
		messages[target] = append(queue, msg)
	}
	delete(messages, user)
}

func Shutdown() {
	shutdown <- struct{}{}
}

func main() {
	log.SetFlags(log.Ltime)

	var listenAddress string
	var serverAddress string
	var username string

	switch len(os.Args) {
	case 2:
		listenAddress = net.JoinHostPort("", os.Args[1])
	case 3:
		serverAddress = os.Args[1]
		if strings.HasPrefix(serverAddress, ":") {
			serverAddress = "localhost" + serverAddress
		}
		username = strings.TrimSpace(os.Args[2])
		if username == "" {
			log.Fatal("empty user name")
		}

	default:
		log.Fatalf("Usage: %s <port>  OR  %s <server> <user>", os.Args[0], os.Args[0])
		//serverAddress = "8080"
		//username = "Jordan"
	}

	if len(listenAddress) > 0 {
		server(listenAddress)
	} else {
		client(serverAddress, username)
	}

}
